package Control;


import View.BankAccount_GUIarea;
import View.BankAccount_GUIlabel;
import View.GUI_Button;
import View.GUI_CheckBox;
import View.GUI_Combobox;
import View.GUI_Menu;
import View.GUI_Radio;
import Model.BankAccount;



public class Main {
	
	public static void main(String[] args){
		new Main();
	}
	 
	  BankAccount bank1; 
	  BankAccount bank2;
	  BankAccount_GUIlabel frame1;
	  BankAccount_GUIarea frame2;
	  
	  public Main()
	  {   
		  bank1 = new BankAccount(50314,"Ruttanagorn");
		  new BankAccount_GUIlabel(bank1);
		  bank2 = new BankAccount(50314,"Ruttanagorn");
		  new BankAccount_GUIarea(bank2);
		  
		  new GUI_Button();
		  new GUI_Radio();
		  new GUI_CheckBox();
		  new GUI_Combobox();
		  new GUI_Menu();
		  
		  
	   }
	  
   	         
	

}
