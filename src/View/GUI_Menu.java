package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;


public class GUI_Menu
{
	public JFrame frame;
	
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 450;
	private JPanel BG;
	private JPanel BGpanel;
	private JMenuBar Menubar;
	private JMenu menu;
	private JMenu color;
	private JMenuItem red;
	private JMenuItem green;
	private JMenuItem blue;
	
	

	
	public GUI_Menu()
	{
		frame = new JFrame("Test Menu");
		createMenuButton();
		createPanel();
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setVisible(true);
		frame.setResizable(false);
	} 
	
	private void createMenuButton(){
		
		Menubar = new JMenuBar();
		menu = new JMenu("Menu");
		color = new JMenu("Color");
		red = new JMenuItem("RED");
		green = new JMenuItem("GREEN");
		blue = new JMenuItem("BLUE");
		color.add(red);
		color.add(green);
		color.add(blue);
		menu.add(color);
		Menubar.add(menu);
		
		red.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				BG.setBackground(Color.RED);
				BGpanel.setBackground(Color.RED);
			}
			});
		green.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				BG.setBackground(Color.GREEN);
				BGpanel.setBackground(Color.GREEN);
			}
			});
		blue.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				BG.setBackground(Color.BLUE);
				BGpanel.setBackground(Color.BLUE);
			}
			});
				
	}
	
	private void createPanel(){
		BG = new JPanel();
		BG = new JPanel();
		BG.setLayout(new BorderLayout());
		frame.setJMenuBar(Menubar);
		frame.add(BG);
		
		
		
	 }
	
}
