package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class GUI_Combobox
{
	public JFrame frame;
	
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 450;
	private JPanel BG;
	private JPanel packButton;
	private JPanel BGpanel;
	private JComboBox select_color;
	public String colors[] = {"Select color","RED","GREEN","BLUE"};
	
	

	
	public GUI_Combobox()
	{
		frame = new JFrame("Test Combo Box");
		createComboButton();
		createPanel();
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setVisible(true);
		frame.setResizable(false);
	} 
	
	private void createComboButton(){
		
		select_color = new JComboBox<String>(colors);
		
		select_color.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				if(select_color.getSelectedIndex()==1){
					BG.setBackground(Color.RED);
					BGpanel.setBackground(Color.RED);
				}
				if(select_color.getSelectedIndex()==2){
					BG.setBackground(Color.GREEN);
					BGpanel.setBackground(Color.GREEN);
				}
				if(select_color.getSelectedIndex()==3){
					BG.setBackground(Color.BLUE);
					BGpanel.setBackground(Color.BLUE);
				}
			}
			});
	}
	
	private void createPanel(){
		BG = new JPanel();
		BGpanel = new JPanel();
		packButton = new JPanel();
		
		BG.setLayout(new BorderLayout());
		packButton.setLayout(new FlowLayout());
		packButton.add(select_color); 
		BG.add(packButton,BorderLayout.SOUTH);
		frame.add(BG);
		
	 }
	
}
