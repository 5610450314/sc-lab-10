package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class GUI_CheckBox
{
	public JFrame frame;
	
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 450;
	private JPanel BG;
	private JPanel packButton;
	private JPanel BGpanel;
	private JCheckBox Red;
	private JCheckBox Green;
	private JCheckBox Blue;
	

	
	public GUI_CheckBox()
	{
		frame = new JFrame("Test CheckBox");
		createCheckButton();
		createPanel();
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setVisible(true);
		frame.setResizable(false);
	} 
	
	private void createCheckButton(){
		
		Red = new JCheckBox("RED");
		Green = new JCheckBox("GREEN");
		Blue = new JCheckBox("BLUE");
		
		Red.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				if(Red.isSelected()){
					BG.setBackground(Color.RED);
					BGpanel.setBackground(Color.RED);
				}
				if(Red.isSelected() && Green.isSelected()){
					BG.setBackground(Color.YELLOW);
					BGpanel.setBackground(Color.YELLOW);
				}
				if(Red.isSelected() && Green.isSelected() && Blue.isSelected()){
					BG.setBackground(Color.GRAY);
					BGpanel.setBackground(Color.GRAY);
				}
				}
			});
		Green.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				if(Green.isSelected()){
					BG.setBackground(Color.GREEN);
					BGpanel.setBackground(Color.GREEN);
				}
				if(Green.isSelected() && Red.isSelected()){
					BG.setBackground(Color.YELLOW);
					BGpanel.setBackground(Color.YELLOW);
				}
				if(Green.isSelected() && Red.isSelected() && Blue.isSelected()){
					BG.setBackground(Color.GRAY);
					BGpanel.setBackground(Color.GRAY);
				}
				}
			});
		Blue.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				if(Blue.isSelected()){
					BG.setBackground(Color.BLUE);
					BGpanel.setBackground(Color.BLUE);
				}
				if(Blue.isSelected() && Red.isSelected()){
					BG.setBackground(Color.PINK);
					BGpanel.setBackground(Color.PINK);
				}
				if(Blue.isSelected() && Red.isSelected() && Green.isSelected()){
					BG.setBackground(Color.GRAY);
					BGpanel.setBackground(Color.GRAY);
				}
				}
			});
	}
	
	private void createPanel(){
		BG = new JPanel();
		BGpanel = new JPanel();
		packButton = new JPanel();
		
		BG.setLayout(new BorderLayout());
		packButton.setLayout(new FlowLayout());
		packButton.add(Red); 
		packButton.add(Green); 
		packButton.add(Blue); 
		BG.add(packButton,BorderLayout.SOUTH);
		frame.add(BG);
		
		
	 }
	
}
