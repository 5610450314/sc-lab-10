package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;


public class GUI_Radio 
{
	public JFrame frame;
	
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 450;
	private JPanel BG;
	private JPanel packButton;
	private JPanel BGpanel;
	private JRadioButton Red;
	private JRadioButton Green;
	private JRadioButton Blue;
	private ButtonGroup Packing;
	

	
	public GUI_Radio()
	{
		frame = new JFrame("Test Radio Button");
		createRButton();
		createPanel();
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setVisible(true);
		frame.setResizable(false);
	} 
	
	private void createRButton()
	{
		Packing = new ButtonGroup();
		
		Red = new JRadioButton("RED");
		Green = new JRadioButton("GREEN");
		Blue = new JRadioButton("BLUE");
		Packing.add(Red);
		Packing.add(Green);
		Packing.add(Blue);
		
		Red.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				BG.setBackground(Color.RED);
				BGpanel.setBackground(Color.RED);
				}
			});
		Green.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				BG.setBackground(Color.GREEN);
				BGpanel.setBackground(Color.GREEN);
				}
			});
		Blue.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				BG.setBackground(Color.BLUE);
				BGpanel.setBackground(Color.BLUE);
				}
			});
	}
	
	private void createPanel(){
		BG = new JPanel();
		BGpanel = new JPanel();
		packButton = new JPanel();
		
		BG.setLayout(new BorderLayout());
		packButton.setLayout(new FlowLayout());
		packButton.add(Red); 
		packButton.add(Green); 
		packButton.add(Blue); 
		BG.add(packButton,BorderLayout.SOUTH);
		frame.add(BG);
		
	 }
	
}
