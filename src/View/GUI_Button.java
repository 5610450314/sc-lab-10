package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class GUI_Button{
	
	public JFrame frame; 
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 450;
	private JPanel BG;
	private JPanel packButton;
	private JPanel BGpanel;
	private JButton Red;
	private JButton Green;
	private JButton Blue;
	

	
	public GUI_Button(){
		frame = new JFrame("Test Button");
		createButton();
		createPanel();
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setVisible(true);
		frame.setResizable(false);
	} 
	
	private void createButton(){
		
		Red = new JButton("RED");
		Green = new JButton("GREEN");
		Blue = new JButton("BLUE");
		
		
		Red.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				BG.setBackground(Color.RED);
				BGpanel.setBackground(Color.RED);
				}
			});
		Green.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				BG.setBackground(Color.GREEN);
				BGpanel.setBackground(Color.GREEN);
				}
			});
		Blue.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event){
				BG.setBackground(Color.BLUE);
				BGpanel.setBackground(Color.BLUE);
				}
			});
	}
	
	private void createPanel(){
		BG = new JPanel();
		BGpanel = new JPanel();
		packButton = new JPanel();
		
		BG.setLayout(new BorderLayout());
		packButton.setLayout(new FlowLayout());
		packButton.add(Red); 
		packButton.add(Green); 
		packButton.add(Blue); 
		BG.add(packButton,BorderLayout.SOUTH);
		frame.add(BG);
		
	 }
	
}
